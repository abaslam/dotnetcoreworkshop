﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection.Services
{
    public interface ITransientScope
    {
        Guid Id { get; }
    }

    public class TransientScope : ITransientScope
    {
        public TransientScope()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; }
    }
}
