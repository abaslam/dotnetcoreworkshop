﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection.Services
{
    public class ProductOracleService : IProductService
    {
        public List<Product> GetProducts()
        {
            return new List<Product>
            {
                new Product{Id=1, Name="oracle 1"},
                new Product{Id=2, Name="oracle 2"},
            };
        }
    }
}
