﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection.Services
{
    public class ProductService : IProductService
    {
        private ITransientScope transient;
        private IRequestScope requestScope;
        private ISingletonScope singleton;
        public ProductService(ITransientScope transient, IRequestScope requestScope, ISingletonScope singleton)
        {
            this.transient = transient;
            this.requestScope = requestScope;
            this.singleton = singleton;
        }
        public List<Product> GetProducts()
        {
            return new List<Product>
            {
                new Product{Id=1, Name="test1"},
                new Product{Id=2, Name="test2"},
            };
        }
    }
}
