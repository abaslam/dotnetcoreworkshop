﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection.Services
{
    public interface IRequestScope
    {
        Guid Id { get;  }
    }

    public class RequestScope : IRequestScope
    {
        public RequestScope()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; }
    }
}
