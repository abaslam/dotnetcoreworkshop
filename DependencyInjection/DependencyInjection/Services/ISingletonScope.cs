﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DependencyInjection.Services
{
    public interface ISingletonScope
    {
        Guid Id { get; }
    }

    public class SingletonScope : ISingletonScope
    {
        public SingletonScope()
        {
            this.Id = Guid.NewGuid();
        }
        public Guid Id { get; }
        
    }
}
