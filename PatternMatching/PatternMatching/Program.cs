﻿using System;

namespace PatternMatching
{
    class Program
    {
        static void Main(string[] args)
        {
            var i = 123_12_31_212;

            Console.WriteLine(i);
            //var simplePatternMatching = new PatternMatchingSimple();
            //simplePatternMatching.Run();

            //var complexPatternMatching = new PatternMatchingComplex();
            //complexPatternMatching.Run();

            //var tupleSample = new TuplesSimple();
            //tupleSample.Run();

            //var tupleComplex = new TuplesComplex();
            //tupleComplex.Run();

            //var localMethods = new LocalMethods();
            //localMethods.Run();

            //var refReturn = new RefReturns();
            //refReturn.Run();

            Console.WriteLine(ThrowExpression("Test"));
            //Console.WriteLine(ThrowExpression(null));
            Console.WriteLine("Hello World!");
            Console.ReadLine();
        }  
        
        public static string ThrowExpression(string name)
        {
            //if (name == null)
            //
            //    throw new ArgumentNullException();
            //}

            //return name;

            return name ?? throw new ArgumentNullException();
        }
    }     

}