﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class TuplesComplex
    {
        public void Run()
        {
            //TupleDictionary();

            (int hour, int min, int sec) = GetTime();
            var (h, m, s) = GetTime();

            int h1;
            int m2;
            int s2;

            (h1, m2, s2) = GetTime();

            Console.WriteLine($"{hour}: {min}: {sec}");
        }

        public void TupleDictionary()
        {
            var tupleDictionary = new Dictionary<(int, int), string>();

            tupleDictionary.Add((100, 20), "Your romms is 20 in 100 floor");

            var result = tupleDictionary[(100, 20)];

            Console.WriteLine(result);
        }

        public (int, int, int) GetTime()
        {
            return (1, 30, 40);
        }
    }
}
