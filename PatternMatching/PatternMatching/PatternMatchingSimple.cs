﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    class PatternMatchingSimple
    {
        public void Run()
        {
            //PrintSum(null);
            //PrintSum("asdas");
            //PrintSum(10);

            PrintSum2(null);
            PrintSum2("asdas");
            PrintSum2("10");
        }


        public void PrintSum(object o)
        {
            if (o is null) // constant pattern
            {
                return;
            }

            if (!(o is int i)) // type pattern (int)
            {
                return;
            }

            int sum = 0;
            for (int j = 0; j <= i; j++)
            {
                sum += j;
            }

            Console.WriteLine($"The sum of 1 to {i} is {sum}");
        }

        public void PrintSum2(object o)
        {
            if ((o is string s && int.TryParse(s, out int j)))
            {

                int sum = 0;
                for (int m = 0; m <= j; m++)
                {
                    sum += m;
                }

                Console.WriteLine($"The sum of 1 to {j} is {sum}");
            }
            
            //if(o is string)
            //{
            //    int m;
            //    int.TryParse(o.ToString(), out m);
            //}
        }
    }
}
