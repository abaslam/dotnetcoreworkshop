﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class TuplesSimple
    {
        public void Run()
        {
            //var hour = GetTime(out int min, out int sec);

            var time = GetTime();

            Console.WriteLine($"{time.Item1}: {time.Item2}: {time.Item3}");

            var namedTime = GetTimeWithName();

            Console.WriteLine($"{namedTime.hour}: {namedTime.min}: {namedTime.sec}");
        }        

        public (int, int, int) GetTime()
        {
            return (1, 30, 40);
        }

        public (int hour, int min, int sec) GetTimeWithName()
        {
            return (1, 30, 40);
        }

        //public int GetTime(out int min, out int sec)
        //{
        //    min = 10;
        //    sec = 20;
        //    return 10;
        //}

    }
}
