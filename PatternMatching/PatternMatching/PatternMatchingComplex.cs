﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class PatternMatchingComplex
    {
        public void Run()
        {
            Employee employee = new VicePresident();
            employee.Salary = 175000;
            employee.Years = 7;
            (employee as VicePresident).NumberManaged = 200;
            (employee as VicePresident).StockShares = 4000;

            switch (employee)
            {
                case VicePresident vp when (vp.StockShares < 5000):
                    Console.WriteLine($"Junior VP with {vp.StockShares}");
                    break;

                case VicePresident vp when (vp.StockShares >= 5000):
                    Console.WriteLine($"Senior VP with {vp.StockShares}");
                    break;

                case Manager m:
                    Console.WriteLine($"Number of people managed {m.NumberManaged}");
                    break;

                case Employee e:
                    Console.WriteLine($"Number of year in Company {e.Years}");
                    break;
            }
        }
    }

    public class Employee
    {
        public int Salary { get; set; }
        public int Years { get; set; }
    }

    public class Manager : Employee
    {
        public int NumberManaged { get; set; }
    }

    public class VicePresident : Manager
    {
        public int StockShares { get; set; }
    }
}
