﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PatternMatching
{
    public class LocalMethods
    {
        public void Run()
        {
            Console.WriteLine(Fibonacci(10));
        }

        public int Fibonacci(int x)
        {
            return Fib(x).current;

            (int current, int previous) Fib(int i)
            {
                if (i == 0)
                {
                    return (1, 0);
                }

                var (current, previous) = Fib(i - 1);
                return ((current + previous), current);
            }

        }


    }
}
