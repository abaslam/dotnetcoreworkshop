﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

namespace Sessions.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {      
            HttpContext.Session.SetString("Name", "Rick");
            HttpContext.Session.SetInt32("Year", 3);

            var employee = new Employee { Name = "Test" };

            HttpContext.Session.Set<Employee>("Emp", employee);

            HttpContext.Items["Test"] = true;

            var test = HttpContext.Items["Test"];
            return View();
        }

        public IActionResult About()
        {
            var name = HttpContext.Session.GetString("Name");
            var year = HttpContext.Session.GetInt32("Year");

            var employee = HttpContext.Session.Get<Employee>("Emp");

            var test = HttpContext.Items["Test"];
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
