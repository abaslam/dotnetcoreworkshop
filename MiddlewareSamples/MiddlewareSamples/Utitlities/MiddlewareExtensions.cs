﻿using Microsoft.AspNetCore.Builder;
using MiddlewareSamples.Middlewares;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MiddlewareSamples.Utitlities
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseRequestCulture(this IApplicationBuilder app)
        {
            return app.UseMiddleware<RequestCultureMiddleware>();
        }
    }
}
